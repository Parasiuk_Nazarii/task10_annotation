import view.MyView;

import java.lang.reflect.InvocationTargetException;

public class Application {
    public static void main(String[] args) throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        new MyView().show();

    }
}
