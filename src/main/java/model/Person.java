package model;

import annotation.PersonAnnotation;
import annotation.StringAnnotation;

@PersonAnnotation(id = "id345987")
public class Person {
    @StringAnnotation
    private String name;
    @StringAnnotation
    private String surname;
    @StringAnnotation
    private String nationality;
    private int age;
    private Gender gender;
    @StringAnnotation
    private String passport;

    public Person(String name, String surname, String nationality, int age, Gender gender, String passport) {
        this.name = name;
        this.surname = surname;
        this.nationality = nationality;
        this.age = age;
        this.gender = gender;
        this.passport = passport;
    }

    public Person() {
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public void says(String str) {
        System.out.println(name + " " + surname + ":  " + str);
    }

    public void talks(String... str) {
        String text = name + " " + surname + "talks:  ";
        for (String s : str) {
            text += s;
        }
        System.out.println(text);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getNationality() {
        return nationality;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Person)) return false;

        Person person = (Person) o;

        return getPassport().equals(person.getPassport());
    }

    @Override
    public int hashCode() {
        return getPassport().hashCode();
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", nationality='" + nationality + '\'' +
                ", age=" + age +
                ", gender=" + gender +
                ", passport='" + passport + '\'' +
                '}';
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getPassport() {
        return passport;
    }

    public void setPassport(String passport) {
        this.passport = passport;
    }
}
